package com.junit.test;

import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.rest.Main;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class WholeTest {
	private String inputNumber;
	   private Integer expectedResult;
	   private Main main;

	   @Before
	   public void initialize() {
	      main = new Main();
	   }

	   public WholeTest(String inputNumber, Integer expectedResult) {
	      this.inputNumber = inputNumber;
	      this.expectedResult = expectedResult;
	   }

	   @Parameterized.Parameters
	   public static Collection primeNumbers() {
	      return Arrays.asList(new Object[][] {
	         { "25767",  263},
	         { "8527741",  55080},
	         { "7177917", 1181},
	         { "2387872",  405790},
	         { "99", 909 },
	         {"-77", 0},
	         {"-5897327", -1720610},
	         {"77", 0},
	         {"43256791", 11331545}
	      });
	   }

	   
	   @Test
	   public void testPrimeNumberChecker() {
	      System.out.println("Parameterized Number is : " + inputNumber);
	      try {
	      int num = main.initialCheck(inputNumber);
	      num = main.firstCheck(num);
	       num = main.secondCheck(num);
	       num = main.thirdCheck(num);
	       num = main.fourthCheck(num);
	      assertEquals(expectedResult, 
	      num, 0.0);
	      } catch (ArithmeticException e) {
	    	  assertEquals(e.getMessage(), ("/ by zero"));
	      } catch(NumberFormatException e) {
	    	  assertEquals(e.getMessage(), ("For input string: " + "\"" + inputNumber + "\""));
	      }
	   }
}
