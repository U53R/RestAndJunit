package com.junit.test;

import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.rest.Main;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Test3 {
	private Integer inputNumber;
	   private Integer expectedResult;
	   private Main main;

	   @Before
	   public void initialize() {
	      main = new Main();
	   }

	   public Test3(Integer inputNumber, Integer expectedResult) {
	      this.inputNumber = inputNumber;
	      this.expectedResult = expectedResult;
	   }

	   @Parameterized.Parameters
	   public static Collection primeNumbers() {
	      return Arrays.asList(new Object[][] {
	         { 256, 256 },
	         { 8526941, 165261841 },
	         { 19, 118 },
	         { 2882, 216162 },
	         { 99, 1818 },
	         {-159, -1518},
	         {589321, 51618321}
	      });
	   }

	   
	   @Test
	   public void testPrimeNumberChecker() {
	      System.out.println("Parameterized Number is : " + inputNumber);
	      assertEquals(expectedResult, 
	     main.secondCheck(inputNumber), 0.0);
	   }
}

