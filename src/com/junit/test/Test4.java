package com.junit.test;

import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.rest.Main;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Test4 {
	private Integer inputNumber;
	   private Integer expectedResult;
	   private Main main;

	   @Before
	   public void initialize() {
	      main = new Main();
	   }

	   public Test4(Integer inputNumber, Integer expectedResult) {
	      this.inputNumber = inputNumber;
	      this.expectedResult = expectedResult;
	   }

	   @Parameterized.Parameters
	   public static Collection primeNumbers() {
	      return Arrays.asList(new Object[][] {
	         { 25767, 256 },
	         { 8527741,  85241},
	         { 71779717, 191 },
	         { 2787872, 2882 },
	         { 99, 99 },
	         {-77, 0},
	         {58973271, 589321},
	         {77, 0}
	      });
	   }

	   
	   @Test
	   public void testPrimeNumberChecker() {
	      System.out.println("Parameterized Number is : " + inputNumber);
	      assertEquals(expectedResult, 
	      main.thirdCheck(inputNumber), 0.0);
	   }
}




