package com.junit.test;

import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.rest.Main;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Test1 {
	private String inputNumber;
	   private Integer expectedResult;
	   private Main main;

	   @Before
	   public void initialize() {
	      main = new Main();
	   }

	   public Test1(String inputNumber, Integer expectedResult) {
	      this.inputNumber = inputNumber;
	      this.expectedResult = expectedResult;
	   }

	   @Parameterized.Parameters
	   public static Collection primeNumbers() {
	      return Arrays.asList(new Object[][] {
	         {"152", 152},
	         { "-124",  -124},
	         {"abc", 0},
	         {"0", 0}
	      });
	   }

	   
	   @Test
	   public void testPrimeNumberChecker() {
	      System.out.println("Parameterized Number is : " + inputNumber);
	      try {
	    	  assertEquals(expectedResult, 
	    		      main.initialCheck(inputNumber), 0.0);
	      } catch(NumberFormatException e) {
	    	  assertEquals(e.getMessage(), ("For input string: " + "\"" + inputNumber + "\""));
	      }
	   }
}

