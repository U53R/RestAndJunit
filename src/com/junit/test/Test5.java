package com.junit.test;

import java.util.Arrays;
import java.util.Collection;
 
import org.junit.Test;
import org.junit.Before;

import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.rest.Main;

import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class Test5 {
	private Integer inputNumber;
	   private Integer expectedResult;
	   private Main main;

	   @Before
	   public void initialize() {
	      main = new Main();
	   }

	   public Test5(Integer inputNumber, Integer expectedResult) {
	      this.inputNumber = inputNumber;
	      this.expectedResult = expectedResult;
	   }

	   @Parameterized.Parameters
	   public static Collection primeNumbers() {
	      return Arrays.asList(new Object[][] {
	         { 25767, 12883 },
	         { 8527741,  2842580},
	         { 2787872, 696968},
	         {-4, -4},
	         {120, 60},
	         {-120, -60},
	         {153, 0}
	      });
	   }

	   
	   @Test
	   public void testPrimeNumberChecker() {
	      System.out.println("Parameterized Number is : " + inputNumber);
	      try {
	    	  int a = main.fourthCheck(inputNumber);
	    	  assertEquals(expectedResult, 
	    		      a, 0.0);
	      } catch(ArithmeticException e) {
	    	  assertEquals(e.getMessage(), ("/ by zero"));
	      }
	   }
}

